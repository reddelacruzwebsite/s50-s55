Attendance
September 06, 2022
===========================
1. Ruby Mae Sargento
2. Gabriel Guillermo
3. Yoby Fernadez
4. Marnel Justine Brobio
5. Glenn Perey
6.Eli Raphael Del Rosario
7. Ronel Tayawa
8. Loren Dela Cruz
9. Emmanuel Garcia
10. Joshua Pascua
11. Jean Victoria Eraya
12. Alexis Vinasoy
13. Maica Ocampo
14. Louies Aronn Dela Cerna
15.  Nessa Agustin
16. Ronel Andaya
17. Red Dela Cruz
18. Jessie Libita
19. Mark Ramos
20. Jay Leizl Reyes
21. Elysha Dumpit
22. Salman Gayampal
23.  Jhon Paul Galela



ATTENDANCE: 09/07/2022
===================================
1. Marnel Justine Brobio
2. Ronel Tayawa
3. Jay Leizl Reyes
4. Glenn Perey
5. Loren Dela Cruz
6. Yoby Fernandez
7. Emmanuel Garcia
8. Jhon Paul Galela
9. Ruby Mae Sargento
10. Red Dela Cruz
11. Angelo De Gracia
12. Alexis Vinasoy
13. Joshua Pascua
13. Jean Victoria Eraya
15. Maica Ocampo
16. Eli Raphael Del Rosario
17. Mark Ramos
18. Louies Aronn Dela Cerna
19. Jessie Libita
20. Salman Gayampal
21.Elysha Dumpit
22. Jesus Carullo
23. Gabriel Guillermo
24. Ronel Andaya



ATTENDANCE: 09/08/2022
=========================
1. Eli Raphael Del Rosario
2. Marnel Justine Brobio
3. Ronel tayawa
4. Gabriel Guillermo
5. Emmanuel Garcia
6. Ruby Mae Sargento
7. Jay Leizl Reyes
8. Alexis Vinasoy
9. Maica Ocampo
10. Glenn Perey
11. Jhon Paul Galela
12. Jesus Carullo
13. Louies Aronn Dela Cerna
14. Salman Gayampal
15. Joshua Pascua
16. Red Dela Cruz
17. Jean Victoria Eraya
18. Mark Ramos
19. Loren Dela Cruz

S39 activity
Carullo - did not apply user authentication in both routes and controllers.

Del Rosario - was able to decode the token but was not able to use is to create a business logic for validating user of he is an admin ot not.

Dela Cruz - used userId for authorization. Inaccurate business logic.

Eraya - token was verified but not decoded to access payload and use the isAdmin property for user validation.

Ocampo - token was verified and decoded but was not passed in the controller function for user valication.

Reyes - route for course creation is correct but arguments were not passed properly to addCourse() parameter. Also no conditional statements composed for user validation.


ATTENDANCE: 09/09/2022
====================
1. Marnel Justine Brobio
2. Emmanuel Garcia
3. Jean Victoria Eraya
4. Jay Leizl Reyes
5. Glenn Perey
6. Loren Dela Cruz
7. Joshua Pascua
8. Ronel Tayawa
9. Alexis Vinasoy
10. Ruby Mae Sargento
11. Yoby Fernandez
12. Angelo De Gracia
13. Jhon Paul Galela
14. Gabriel Guillermo
15 Maica Ocampo
16. Mark Ramos
17. Salman Gayampal
18. Red Dela Cruz
19. Jesus Carullo
20. Elysha Dumpit
21. Ronel Andaya


ATTENDANCE: 09/12/2022
====================
1. Yoby Fernandez
2. Jean Victoria Eraya
3. Eli Raphael Del Rosario
4. Marnel Justine Brobio
5. Glenn Perey
6. Jhon Paul Galela
7. Ronel Tayawa
8. Loren Dela Cruz
10. Jay Leizl Reyes
11. Jesus Carullo
12. Louies Aronn Dela Cerna
13. Alexis Vinasooy
14. Joshua Pascua
15. Emmanuel Garcia
16. Red Dela Cruz
17 Maica Ocampo
18. Salman Gayampal
19. Nessa Agustin
20. Gabriel Guillermo
21. Mark Ramos
22. Angelo De Gracia
23. Elysha Dumpit
24. Ronel Andaya

Heroku log in instructions:

terminal:
heroku -v ---- to check the version
heroku login ---- after loging in the browser
click enter to proceed.npm install
terminal will confirm if your heroku accnt is logged in and connected to the terminal

heroku login -i ---- can be used to log in email and password directly in the terminal if it can't be logged in thru the browser

once logged in press ctrl + c
the create your app

heroku create - to create heroku app
//Use the first link on Postman + the endpoint from index and endpoint from routes
https://vast-retreat-49000.herokuapp.com/


new app for course booking API test:

https://pacific-harbor-85936.herokuapp.com/ | https://git.heroku.com/pacific-harbor-85936.git



 | https://git.heroku.com/vast-retreat-49000.git
once the 2 links were created copy and save them on a text file.
then:
git push heroku master to initialize app
then use the first link 
in Doploying your app in postman

