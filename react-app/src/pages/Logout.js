import { useContext, useEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';



export default function Logout(){

    // Consume the UserContext object and destructure it to access the user setUser function unsetUser function from the context provider.

    const {unsetUser, setUser} = useContext(UserContext);


    // Function to clear User's infomation from the localStorage
    unsetUser();
    //clears the data in local storage when logged out
    // localStorage.clear();

    useEffect(() => {
        setUser({id: null})
    }, [])

    return(
        // Redirect / Navigate the user back to the  homepage
        <Navigate to="/"/>

    )
};