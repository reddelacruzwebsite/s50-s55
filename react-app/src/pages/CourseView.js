import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'; 



export default function CourseView() {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();

  const { courseId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

  const enroll = (courseId) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/enroll`, {
      method: "POST",
      headers: {
        'Content-Type': "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        courseId: courseId
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)

      if(data === true) {
        Swal.fire({
        title: "Successfully Enrolled!",
        icon: "success",
        text: "You have successfully enrolled to this course."
      })

        navigate("/courses");

      } else {
        Swal.fire({
        title: "Something went wrong.",
        icon: "error",
        text: "Please try again."
      })
      }
    })
  }

  useEffect(() => {
    console.log(courseId);

    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
    .then(res => res.json())
    .then(data => {
      console.log(data);

      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);
    })
  }, [courseId])

	return (
		<Row>
			    <Col className="p-0">
      <Card className="m-2">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>
             <span className="my-1 d-flex"><b>Description</b></span>
             {description}<br/>
            <span className="my-1 d-flex"><b>Price</b></span>
             {price}<br/>
             <span className="my-1 d-flex"><b>Class Schedule:</b></span>
             5:30PM - 9:30PM<br/>
            </Card.Text>


            {
              (user.id !== null) ? 
              <Button className="bg-primary" onClick={() => enroll(courseId)}>Enroll</Button>
              :
              <Button className="bg-primary" as={Link} to="/login">Log in to enroll</Button>
              
            }


          </Card.Body>
        </Card>
    </Col>
    </Row>
		)
}