import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

const navigate = useNavigate();

const { user, setUser } = useContext(UserContext); 
 console.log(user.email)

const [email, setEmail] = useState("");
const [password1, setPassword1] = useState("")
const [password2, setPassword2] = useState("")
const [firstName, setFirstName] = useState("")
const [lastName, setLastName] = useState("")
const [mobileNo, setmobileNo] = useState("")

const [isActive, setIsActive] = useState(false);
const [newUser, setNewUser] = useState("");
// console.log(email);
// console.log(password1);
// console.log(password2);

function registerUser (e) {
	e.preventDefault();




	// alert("Thank you for registering")


      fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {

    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        email: email

    })

      })
      .then(res => res.json())
      .then(data => {
        console.log(data);

             if(data == true) {
                  Swal.fire({
        title: "Email already exists",
        icon: "error",
        text: "Please check your email and try again!"
      })
             } else {
   fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password1

    })

  })
  .then(res => res.json())
  .then(data => {

    if(typeof data !== "undefined") {

  setNewUser({
        firstName: firstName,
        lastName: lastName,
        email: email,
        mobileNo: mobileNo,
        password: password1
    })


      Swal.fire({
        title: "Thank you for registering.",
        icon: "success",
        text: "Welcome to Zuitt!"
      })

      navigate("/login");
    } else {
       Swal.fire({
        title: "Error found",
        icon: "error",
        text: "Please check your details and try again."
      })
    }
  })
      
             }
            


      })



  // Clear input fields
  setFirstName("");
  setLastName("");
  setEmail("");
  setmobileNo("");
  setPassword1("");
  setPassword2("");





}




useEffect(() => {
	if((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {

		setIsActive(true)
	} else {
		setIsActive(false)
	}

}, [firstName, lastName, email, mobileNo, password1, password2])

console.log(user.email !== null)

return (

   (user.id !== null) ?
           <Navigate to="/courses"/>
      : 

<Form onSubmit={(e) => registerUser(e)}>
	 <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First name</Form.Label>
        <Form.Control 
        type="text" 
        value={firstName}
        onChange={(e) => {setFirstName(e.target.value)}}
        placeholder="Enter your first name" />
      </Form.Group>

      <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last name</Form.Label>
        <Form.Control 
        type="text" 
        value={lastName}
        onChange={(e) => {setLastName(e.target.value)}}
        placeholder="Enter your last name" />
      </Form.Group>



      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        value={email}
        onChange={(e) => {setEmail(e.target.value)}}
        placeholder="Enter email" />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile number</Form.Label>
        <Form.Control 
        type="text" 
        value={mobileNo}
        onChange={(e) => {setmobileNo(e.target.value)}}
        placeholder="Enter your mobile number" />
      </Form.Group>

      <Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        value={password1}
        onChange={(e) => {setPassword1(e.target.value)}}
        placeholder="Enter password" />
      </Form.Group>

       <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Re-enter password</Form.Label>
        <Form.Control 
        type="password" 
        value={password2}
        onChange={(e) => {setPassword2(e.target.value)}}
        placeholder="Re-enter password" />
      </Form.Group>
  
      {isActive ?
		      <Button variant="primary" type="submit" id="submitBtn">
		        Submit
		      </Button> 
		      : 
		      <Button variant="primary" type="submit" id="submitBtn" disabled>
		        Submit
		      </Button> 

      }

	


    </Form>

	)

	}