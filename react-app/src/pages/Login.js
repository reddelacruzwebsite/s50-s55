import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

const { user, setUser } = useContext(UserContext);  

const [email, setEmail] = useState("");
const [password, setPassword] = useState("")

const [isActive, setIsActive] = useState(false);


function loginUser (e) {
	e.preventDefault();

  fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
    method: "POST",
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
       email: email,
       password: password

    })

  })
  .then(res => res.json())
  .then(data => {

    console.log(data);
    console.log(data.access);

    if(typeof data.access !== "undefined") {
      localStorage.setItem('token', data.access)
      retrieveUserDetails(data.access)

      Swal.fire({
        title: "Login successful",
        icon: "success",
        text: "Welcome to Zuitt!"
      })
    } else {
       Swal.fire({
        title: "Authentication failed",
        icon: "error",
        text: "Please check your login details and try again."
      })
    }
  })

// comment out later
  // localStorage.setItem("email", email)
  // setUser({email: localStorage.getItem('email')})



  // alert("You are now logged in.")
	setEmail("");
	setPassword("");

console.log(`${email} has been verified! Welcome back!`)
}


const retrieveUserDetails = (token) => {
  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {

    headers: {
    Authorization: `Bearer ${ token }`
    }
  })
  .then(res => res.json())
  .then(data => {

    console.log(data)

    setUser({
      id: data._id,
      isAdmin: data.isAdmin
    })
  })
}


useEffect(() => {
	if(email !== "" && password !== "") {

		setIsActive(true)
	} else {
		setIsActive(false)
	}

}, [email, password])


return (

      (user.id !== null) ?
           <Navigate to="/courses"/>
      :    
      
      <Form onSubmit={(e) => loginUser(e)}>
  
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        type="email" 
        value={email}
        onChange={(e) => {setEmail(e.target.value)}}
        placeholder="Enter email" />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        type="password" 
        value={password}
        onChange={(e) => {setPassword(e.target.value)}}
        placeholder="Enter password" />
      </Form.Group>

      {isActive ?
          <Button variant="primary" type="submit" id="submitBtn">
            Login
          </Button> 
          : 
          <Button variant="primary" type="submit" id="submitBtn" disabled>
            Login
          </Button> 

      }

    </Form>  


	)

	}