import { Fragment, useEffect, useState } from 'react';

import {Row} from 'react-bootstrap';
import CourseCard from '../components/CourseCard';
// import coursesData from '../data/coursesData';


export default function Courses() {

const [courses, setCourses] = useState([]);

	// console.log(coursesData);
	// 	console.log(coursesData[0]);

		// const courses = coursesData.map(course => {
		// 	return (
		// 		<CourseCard key={course.id} courseProp={course}/>
		// 	)
		// })


		useEffect(() => {
			fetch(`${process.env.REACT_APP_API_URL}/courses/`)
			.then(res => res.json())
			.then(data => {
				console.log(data);

				setCourses(data.map(course => {
					return (
							<CourseCard key={course._id} courseProp={course}/>
						)
				}))

			})
		}, [])

	return (
		<Fragment>
   
			<h1>Courses</h1>
		<Row className="d-flex">
			{courses}
			</Row>
		</Fragment>

		)
}