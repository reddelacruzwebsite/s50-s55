import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import CourseCard from '../components/CourseCard';
import Resources from '../components/Resources';


import { Col } from 'react-bootstrap';


import bannerData from '../data/bannerData';

export default function Home() {

	  const banners = bannerData.map(banner => {
            return (
                <Banner key={banner.id} bannerProp={banner}/>
            )
        })

	return (
			<Fragment>
			{banners[0]}
			<Highlights/>
			{/*<CourseCard/>*/}
			<Resources/>
			
			</Fragment>
		)
}

