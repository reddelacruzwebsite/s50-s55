import { Fragment } from 'react';
import Banner from '../components/Banner';

import { Col } from 'react-bootstrap';

import bannerData from '../data/bannerData';

export default function PageNotFound() {

	 const banners = bannerData.map(banner => {
            return (
                <Banner key={banner.id} bannerProp={banner}/>
            )
        })

	return (
			<Fragment>
			{banners[1]}
			
			</Fragment>
		)
}
