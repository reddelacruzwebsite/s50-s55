const bannerData = [

	{
		id: "banner001",
		heading: "Zuitt Coding Bootcamp",
		content: "Opportunities for everyone, everywhere.",
		buttonText: "Enroll Now",
		buttonLink: "/courses"
	},
	{
		id: "banner002",
		heading: "Error 404",
		content: "This page does not exist.",
		buttonText: "Go back to Homepage",
		buttonLink: "/"
	}
]

export default bannerData;