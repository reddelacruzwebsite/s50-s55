// import logo from './logo.svg';
import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';


// components
import AppNavbar from './components/AppNavbar';

//pages
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import PageNotFound from './pages/PageNotFound';

import './App.css';
import { UserProvider } from './UserContext';


function App() {

  const [user, setUser] = useState({
    // email: localStorage.getItem("email")
    id: null,
    isAdmin: null
  })
  
  const unsetUser = () => {
    localStorage.clear();
  }

// ${process.env.REACT_APP_API_URL}

useEffect(() => {
  fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem('token')}`
    }
  })
  .then(res => res.json())
  .then(data => {
    if(typeof data._id !== "undefined") {
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      }) } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    
  })
}, [])


  return (
  
     <UserProvider value={{user, setUser, unsetUser}}>
          <Router>
            <AppNavbar/>
              <Container>
                   <Routes>
                      <Route path="/" element={<Home/>} />
                      <Route path="/courses" element={<Courses/>} />    
                        <Route path="/courses/:courseId" element={<CourseView/>} />      

                        <Route  path="/register" element={<Register/>}></Route>



                      <Route path="/login" element={<Login/>} /> 
                      <Route path="/logout" element={<Logout/>} />     
                      <Route path="/*" element={<PageNotFound/>} />                
                   </Routes>
              </Container>
          </Router>
     </UserProvider>

  );
}

export default App;
