
import { Row, Accordion, Button, div } from 'react-bootstrap';

function showResources() {
  return (

    <Accordion className="my-5">
      <Accordion.Item eventKey="0">
        <Accordion.Header>Mongo DB</Accordion.Header>
        <Accordion.Body className="my-4 px-3">
        <div>MongoDB is a source-available cross-platform document-oriented database program. Classified as a NoSQL database program, MongoDB uses JSON-like documents with optional schemas. MongoDB is developed by MongoDB Inc. and licensed under the Server Side Public License which is deemed non-free by several distributions.</div>
          <Button href="https://www.mongodb.com/" variant="dark">Learn more</Button>
        </Accordion.Body>
      </Accordion.Item>

      <Accordion.Item eventKey="1">
        <Accordion.Header>Node JS</Accordion.Header>
        <Accordion.Body className="my-4 px-3">
          <div>Node.js is an open-source, cross-platform, back-end JavaScript runtime environment that runs on a JavaScript Engine and executes JavaScript code outside a web browser, which was designed to build scalable network applications.</div>
          <Button href="https://nodejs.org/en/" variant="dark">Learn more</Button>
        </Accordion.Body>
      </Accordion.Item>

      <Accordion.Item eventKey="2">
        <Accordion.Header>React JS</Accordion.Header>
        <Accordion.Body className="my-4 px-3">
           <div>React is a JavaScript library for building user interfaces. Learn what React is all about on our homepage or in the tutorial.</div>
          <Button href="https://reactjs.org/docs/getting-started.html" variant="dark">Learn more</Button>
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>

  );
}

export default showResources;