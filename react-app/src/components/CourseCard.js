import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {

// console.log(courseProp);
// console.log(typeof courseProp)

  const { name, description, price, _id } = courseProp;

/*
  Syntax:
  const [getter, setter] = useState(initialGetterValue)
*/

// state hook to store the state of enrollees
const [count, setCount] = useState(0)
const [seats, setSeatCount] = useState(30)


function enroll() {
  // if (seats == 0) {
  //   alert(`No more seats for the ${name} course`)
  // } else 
  if (seats > 0) {
      setCount(count + 1) 

      console.log('Enrollees ' + count)
      setSeatCount(seats - 1) 
  }
}


useEffect(() => {
  if(seats === 0) {
    alert("No more seats available.")
  }

}, [seats])


  return (
 
    <Col className="p-0" xs={12} md={4}>
      <Card className="m-2">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>
             <span className="my-1 d-flex"><b>Description</b></span>
             {description}<br/>
            <span className="my-1 d-flex"><b>Price</b></span>
             {price}<br/>
             <span className="my-1 d-flex"><b>Enrollees:</b></span>
             {count}<br/>
            </Card.Text>
            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>Details</Button>
          </Card.Body>
        </Card>
    </Col>


    )
  }









// original code for seats and count - no 'useffect' - line 22

// function enroll() {
//   if (seats == 0) {
//     alert(`No more seats for the ${name} course`)
//   } else {
//       setCount(count + 1) 

//       console.log('Enrollees ' + count)
//       setSeatCount(seats - 1) 
//   }
// }


