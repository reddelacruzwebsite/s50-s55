import { Fragment } from 'react';
import { Row, Col, Button } from 'react-bootstrap';




export default function Banner({bannerProp}) {

        const { heading, content, buttonText, buttonLink } = bannerProp;

    return (
<>
                <h1>{heading}</h1>
                <p>{content}</p>

               <Button variant="primary" href={buttonLink}>{buttonText}</Button>
</>

        )
}